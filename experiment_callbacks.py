#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May  2 04:47:39 2019

@author: nposocco
"""


from keras import callbacks
import os
import csv
import numpy as np
import warnings

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec

class LossHistory(callbacks.Callback):

   
    def __init__(self, experiment):

        #parameter of the csv and plot		        
        self.reports = [{'type': 'text',
        		'file': 'evolution.csv',
        		'outputs': [0],
        		'order': ['metric', 'set', 'output']},
                {'type': 'plot',
        		'file': 'plot.png',
        		'outputs': [0],
        		'order': ['metric', 'set', 'output']}]
        self.experiment = experiment
        metrics = []
        		        
        self.dimSpecs = {'set': ['', 'val_'], 'output': [0], 'metric': ['loss'] + metrics}
        
        outputs = ['']

#        addVars = [{'name': 'loss', 'kerasName': 'loss', 'val': []},
#                    {'name': 'val_loss', 'kerasName': 'val_loss', 'val': []}]
                    
        addVars = []
                    
#        addNesting = [('overall loss', [('overall loss', [0, 1])])]
        
        addNesting = []
        
        def getKerasName(v):
            
            return '%s%s%s' % (v['set'], outputs[v['output']], v['metric'])
        
                
        for r in self.reports:
            
            # list of variables to monitor
            rvars = []
            
            # nesting: primarily for plots (to e.g. plot several variables on the same plot)
            nesting = []
            
            # only consider specified outputs
            dims = self.dimSpecs.copy()
            dims['output'] = r['outputs']
            
            dimSizes = [len(dims[d]) for d in r['order']]
            
            for d1 in xrange(dimSizes[0]):
                
                for d2 in xrange(dimSizes[1]):
                    
                    for d3 in xrange(dimSizes[2]):
                        
                        idx = [d1, d2, d3]
                        v = [(r['order'][i], dims[r['order'][i]][idx[i]]) \
                            for i in xrange(3)]
                                
                        if d2 == 0 and d3 == 0:
                            nesting.append( (v[0][1], []) )

                        if d3 == 0:
                            nesting[d1][1].append( (v[1][1], []) )
                            
                        nesting[d1][1][d2][1].append(len(rvars)+len(addVars))
                            
                        v = dict(v)

                        v.update({'kerasName': getKerasName(v),
                                  'name': '%s%s_%d' % (v['set'], v['metric'], v['output']), 'val': []})

                        rvars.append(v)
                        
            r['vars'] = addVars + rvars
            r['nesting'] = addNesting + nesting
            


    def on_epoch_end(self, epoch, logs = {}):
        
        for r in self.reports:

            for idx, v in reversed(list(enumerate(r['vars']))):
                
                # save variables into the report objects

                if v['kerasName'] in logs.keys():
                    v['val'].append(logs[v['kerasName']])
                elif epoch == 0:
                    print(idx)
                    del r['vars'][idx]

            # report

            if r['type'] == 'text':
                self.writeCSV(r, logs, True if epoch == 0 else False)
            elif r['type'] == 'plot':
                self.plot(r, logs)


    def writeCSV(self, report, logs, rewrite = False):
    
        with open(os.path.join(self.experiment.getCJD(),'evolution.csv'), "w" if rewrite else "a") as myfile:
            
            writer = csv.writer(myfile, delimiter=';')
            
            rvars = report['vars']
            
            if rewrite:
                writer.writerow([rvar['name'] for rvar in rvars])

            writer.writerow([rvar['val'][-1] for rvar in rvars])              


    def plot(self, report, logs):
        
        colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k']
        lineStyle = ['dashed', 'solid', 'dotted']
        
        rvars = report['vars']
        nesting = report['nesting']

        plt.figure(figsize=(10, 10*len(nesting)))
#        plt.figure(figsize=(20, 60))
        gs = gridspec.GridSpec(len(nesting), 1, height_ratios=[1]*(len(nesting))) 

        # plot
        for i1, l1 in enumerate(nesting):

            ax = plt.subplot(gs[i1])
            plt.title(l1[0])
            plt.ylabel(l1[0])

            for item in [ax.title, ax.xaxis.label, ax.yaxis.label]:
                item.set_fontsize(30)
 
            for item in (ax.get_xticklabels() + ax.get_yticklabels()):
                item.set_fontsize(20)
           
            # color
            for i2, l2 in enumerate(l1[1]):

                # line fill
                for i3, l3 in enumerate(l2[1]):
                    
                    rvars[l3]['val']

                    line, = plt.plot(range(0, len(rvars[l3]['val'])), rvars[l3]['val'], \
                        ls = lineStyle[i3], color = colors[i2], label = rvars[l3]['name'])

            # Shrink current axis by 20%
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
            
            # Put a legend to the right of the current axis
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize = 20)
                    
#            ax.legend(bbox_to_anchor=(1.2, 0.5), fontsize = 20)


        try:

            plt.savefig(os.path.join(self.experiment.getCJD(), report['file']))

        except Exception as inst:

            print(type(inst))
            print(inst)

        plt.close()
        
        
        
class LossPlateauDetection(callbacks.Callback):
    """Stop training when a monitored quantity has stopped improving.
    # Arguments
        monitor: quantity to be monitored.
        min_delta: minimum change in the monitored quantity
            to qualify as an improvement, i.e. an absolute
            change of less than min_delta, will count as no
            improvement.
        patience: number of epochs with no improvement
            after which training will be stopped.
        verbose: verbosity mode.
        baseline: Baseline value for the monitored quantity to reach.
            Training will stop if the model doesn't show improvement
            over the baseline.
    """

    def __init__(self,
                 experiment,
                 monitor='val_loss',
                 min_delta=0,
                 patience=0,
                 verbose=0,
                 baseline=None):
        super(LossPlateauDetection, self).__init__()

        self.experiment = experiment
        
        self.monitor = monitor
        self.baseline = baseline
        self.patience = patience
        self.verbose = verbose
        self.min_delta = min_delta
        self.wait = 0
        self.convergence_epoch = None
        self.convergence_loss = None
        self.best_weights = None

        self.monitor_op = np.less
        self.min_delta *= -1
        
        self.detected_plateau = False

    def on_train_begin(self, logs=None):
        # Allow instances to be re-used
        self.wait = 0
        self.stopped_epoch = 0
        if self.baseline is not None:
            self.best = self.baseline
        else:
            self.best = np.Inf if self.monitor_op == np.less else -np.Inf

    def on_epoch_end(self, epoch, logs=None):
        current = self.get_monitor_value(logs)
        if current is None:
            return
        
        if self.convergence_epoch != None :
            if self.convergence_value - current > 0.5 and not self.detected_plateau :
                self.detected_plateau = True
                if self.verbose > 0:
                    print("Plateau detected")
        if self.monitor_op(current - self.min_delta, self.best):
            self.best = current
            self.wait = 0
        else:
            self.wait += 1
            if self.wait >= self.patience:
                self.convergence_epoch = epoch
                self.convergence_value = self.best

    def on_train_end(self, logs=None):
        if self.detected_plateau :
            self.experiment.saveValue("plateauDetected",True)
        else :
            self.experiment.saveValue("plateauDetected",False)

    def get_monitor_value(self, logs):
        monitor_value = logs.get(self.monitor)
        if monitor_value is None:
            warnings.warn(
                'Plateau Detection conditioned on metric `%s` '
                'which is not available. Available metrics are: %s' %
                (self.monitor, ','.join(list(logs.keys()))), RuntimeWarning
            )
        return monitor_value
