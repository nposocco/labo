#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 10:55:42 2019

@author: nposocco
"""

from labo.experiments import LabTechnician




if __name__ == "__main__" : 

    try :
        # name of target experiment
        name = sys.argv[1].split("=")[1]

        # set this to True if you want the worker to restart failed job attempts
        name = eval(sys.argv[2].split("=")[1])
    except :
        raise Exception("Use the syntax : $ python startWorker.py experiment=<> rebootErrors=<>")
    
    worker = LabTechnician(name)
    worker.work(rebootErrors=rebootErrors)
