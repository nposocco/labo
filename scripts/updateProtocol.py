#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 13:57:44 2019

@author: nposocco
"""

import sys

from labo.experiments import LabTechnician



if __name__ == "__main__" :

    try :
        # name of target experiment
        name = sys.argv[1].split("=")[1]
    except :
        raise Exception("Use the syntax : $ python updateProtocol.py experiment=<>")
    
    worker = LabTechnician(name)
    worker.updateProtocol()
