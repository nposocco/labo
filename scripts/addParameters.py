# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 14:29:37 2019

@author: nposocco
"""

import sys

# dict string parser
import ast

from labo.experiments import LabTechnician




if __name__ == "__main__" : # $ python addParameters.py experiment=<> experimentParams=<> amount=<>

    try :
        # name of target experiment
        name = sys.argv[1].split("=")[1]

        # parameters to add : {<PARAMETER 0>: [<VALUE 00>, ...], <PARAMETER 1>: [<VALUE 01>, ...], ...}
        experimentParams = ast.literal_eval(sys.argv[2].split("=")[1])

        # amount of duplicated experiments (use 1 of deterministic results, 3 if observations depends on random factors and you need robustness on observations)
        amount = int(sys.argv[3].split("=")[1])
    except :
        raise Exception("Use the syntax : $ python addParameters.py experiment=<> experimentParams=<> amount=<>")
    
    worker = LabTechnician(name)
    worker.addAllExperimentCombinationsWith(experimentParams,
                                            amount=amount)
