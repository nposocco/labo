# Imports here
# <IMPORTS>

def protocol(experiment,<ARGUMENTS HERE>) :

    # Good to know :
    #
    # experiment.saveValue(value)
    # -> saves in CSVreport the value <value> in the cell at column <column> in current job's row
    #
    # experiment.getCJD() OR experiment.getCurrentJobDirectory()
    # -> returns path to current job directory
    #
    # experiment.saveModel(model)     with model being a keras model
    # -> saves a png and a json representation of the given model in current job directory
    
    # Create sets via indexes
    # trainIndex = experiment.loadDataIndex(<PATH TO CSV INDEX>,"train")
    # testIndex  = experiment.loadDataIndex(<PATH TO CSV INDEX>,"test")
    # validIndex = experiment.loadDataIndex(<PATH TO CSV INDEX>,"valid")
    # (X_train, Y_train), (X_valid, Y_valid) = experiment.createSetFromIndexes(<PATH TO DATASET>,[trainIndex,validIndex],'x',labels_type,shape,normalization)

