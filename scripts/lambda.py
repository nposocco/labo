# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 14:29:37 2019

@author: nposocco
"""

from __future__ import division,print_function

from labo.experiments import LabTechnician

import matplotlib.pyplot as plt
from math import sqrt,exp,pi
from sklearn.linear_model import LinearRegression
import numpy as np
import pandas as pd
import os





def detectPlateaux(experiment) :
    
    def twoDify(l) :
        return np.array([[elem] for elem in l])
    
    def detectPlateau(lossDf,patience=240,delta=0.4,savePath=None) :
    
        Y = lossDf['val_loss_0'].values
        X = np.array([i for i in range(Y.shape[0])])
        
        # Phase 1
        # "Early Stopping" criteria
        # at the end of the for loop
        #   if there is no sudo-convergence detected
        #      convergenceIndex == 0
        #   else
        #      convergenceIndex contains the index of the begining of the "early stopping" detection
        patienceAmount = patience
        convergenceIndex = None
        reference = 0
        for i in range(1,Y.shape[0]) :
            if abs(Y[i]-Y[reference]) < delta :
                patience -= 1
            else :
                patience = patienceAmount
                reference = i
            if patience == 0 :
                convergenceIndex = reference
                break
    
        # Phase 2
        # if a pseudo-convergence has been detected during Phase 1
        #    we model the pseudo-convergence as a line
        #    we get the minimum point after the pseudo-convergence period
        #    if the likelihood of it being sampled from the line is low enough
        #       we conclude pseudo-convergence wasn't convergence, and thus pseudo-convergence was actualy a plateau
        if convergenceIndex != None :
            # getting the minimum and its index after pseudo-convergence
            YminimumThen = min(list(Y[convergenceIndex:]))
            XminimumThen = list(Y[convergenceIndex:]).index(YminimumThen)+convergenceIndex
            
            # calculating a linear regression on the patience period
            Xreg = twoDify(X[convergenceIndex:convergenceIndex+patienceAmount])
            Yreg = twoDify(Y[convergenceIndex:convergenceIndex+patienceAmount])
            print(Xreg.shape,Yreg.shape)
            reg = LinearRegression().fit(Xreg, Yreg)
    
            # estimating the variance of the gaussian 
            mu = np.mean(Y[convergenceIndex:convergenceIndex+patienceAmount]) # OK
            
            unSurNm1 = 1/(patienceAmount-1)
            sum_ = np.transpose(twoDify(Y[convergenceIndex:convergenceIndex+patienceAmount-1])-mu).dot(twoDify(Y[convergenceIndex:convergenceIndex+patienceAmount-1])-mu)
            sigma = sqrt(unSurNm1*sum_[0,0])
            print(sigma)
            
            likelihood = 1/(sigma*sqrt(2*pi))*exp(-(YminimumThen-3*sigma-mu)**2/(2*sigma**2))
            
            if savePath :
                fig=plt.figure(figsize=(18, 9), dpi= 80, facecolor='w', edgecolor='k')
    
                plt.subplot(1, 1, 1)
                plt.plot(X,Y,color="blue")
                
                # ploting the line showing the epoch of the start of the plateau
                plt.axvline(x=convergenceIndex,color="red")
    
                # ploting the regrassion line (red on the trained data, orange for generalization)
                plt.plot(twoDify(X[convergenceIndex:convergenceIndex+patienceAmount]),reg.predict(twoDify(Y[convergenceIndex:convergenceIndex+patienceAmount])),color="red")
                plt.plot(twoDify(X[convergenceIndex+patienceAmount:X.shape[0]]),reg.predict(twoDify(Y[convergenceIndex+patienceAmount:X.shape[0]])),color="orange")
    
                # ploting lines at distance 1 std
                plt.plot(twoDify(X[convergenceIndex+patienceAmount:X.shape[0]]),reg.predict(twoDify(Y[convergenceIndex+patienceAmount:X.shape[0]]))-1*sigma,color="green")
                plt.plot(twoDify(X[convergenceIndex+patienceAmount:X.shape[0]]),reg.predict(twoDify(Y[convergenceIndex+patienceAmount:X.shape[0]]))+1*sigma,color="green")
    
                # ploting lines at distance 2 std
                plt.plot(twoDify(X[convergenceIndex+patienceAmount:X.shape[0]]),reg.predict(twoDify(Y[convergenceIndex+patienceAmount:X.shape[0]]))-2*sigma,color="green")
                plt.plot(twoDify(X[convergenceIndex+patienceAmount:X.shape[0]]),reg.predict(twoDify(Y[convergenceIndex+patienceAmount:X.shape[0]]))+2*sigma,color="green")
    
                # ploting lines at distance 3 std
                plt.plot(twoDify(X[convergenceIndex+patienceAmount:X.shape[0]]),reg.predict(twoDify(Y[convergenceIndex+patienceAmount:X.shape[0]]))-3*sigma,color="green")
                plt.plot(twoDify(X[convergenceIndex+patienceAmount:X.shape[0]]),reg.predict(twoDify(Y[convergenceIndex+patienceAmount:X.shape[0]]))+3*sigma,color="green")
                
                # ploting the line showing the epoch of the minimum reached after pseudo-convergence
                plt.axvline(x=XminimumThen,color="purple")
                
                plt.title("Plateau")
                fig.savefig(savePath)
            
            if likelihood < 0.05 :
                return True
            else :
                return False
                    
                
        else :
            if savePath :
                fig=plt.figure(figsize=(18, 9), dpi= 80, facecolor='w', edgecolor='k')
    
                plt.subplot(1, 1, 1)
                plt.plot(X,Y,color="blue")
                
                plt.title("NO Plateau")
                fig.savefig(savePath)
            return False
        
    lossDf = pd.read_csv(os.path.join(experiment.getCJD(),"evolution.csv"),sep=";")
    savePath = experiment.getCJD()
    
    return {"plateauDetected2":detectPlateau(lossDf,patience=240,delta=0.4,savePath=savePath)}


if __name__ == "__main__" :   

    name = "TEST0"
    
    worker = LabTechnician(name)
    worker.execute(detectPlateaux)
