# -*- coding: utf-8 -*-
"""
Created on Fri Jul 05 12:00:00 2019

@author: nposocco
"""


def freefall(m, t, material) :

    # simply returns the theoretical newton speed at time t
    
    g = 9.81 #N

    if t < 3 :
        return -g*t
    else :
        return 0
