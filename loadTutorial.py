#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 15:52:35 2019

@author: nposocco
"""
# used to list scripts in labo/scripts/ directory
import os

# used to copy the different labo command scripts
import shutil

if __name__ == "__main__" :
    
    sourcePath = os.path.join(os.getcwd(),"labo","examples","tutorial")
    targetPath = os.getcwd()
    os.remove(os.path.join(targetPath,"protocol.py"))
    for script in os.listdir(sourcePath) :
        shutil.copy2(os.path.join(sourcePath,script),os.path.join(targetPath,script))
