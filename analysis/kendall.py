import numpy as np
import pandas as pd

def getKendallCoeffs(dataframe,features,targets) :
    
    # checking inputs
    assert type(dataframe) is pd.DataFrame
    
    assert type(features) is list and len(features) > 0
    assert all(type(feature) is str for feature in features)
    assert all(feature in dataframe.columns for feature in features)
    
    assert type(targets) is list and len(targets) > 0
    assert all(type(target) is str for target in targets)
    assert all(target in dataframe.columns for target in targets)  
    
    # Changing the dataframe so that catagorical variables are compatible with the algorithm
    df = dataframe.copy()
    df = df[features+targets]
    for column in features :
        if df[column].dtype == np.object :
            df[column] = pd.Categorical(df[column])
            dfDummies = pd.get_dummies(df[column], prefix = column,prefix_sep="=")
            addedFeatures = dfDummies.columns
            df = pd.concat([df, dfDummies], axis=1).drop(column, axis=1)
            features.remove(column)
            features = features + list(addedFeatures)
    for column in targets :
        if df[column].dtype == np.object :
            df[column] = pd.Categorical(df[column])
            dfDummies = pd.get_dummies(df[column], prefix = column,prefix_sep="=")
            addedTargets = dfDummies.columns
            df = pd.concat([df, dfDummies], axis=1).drop(column, axis=1)
            targets.remove(column)
            targets = targets + list(addedTargets)
    
    # calculating Spearman's rank correlations
    res = {}
    for target in targets :
        res[target] = {}
        correlations = df.drop(targets, axis=1).apply(lambda x: x.corr(df[target],method="kendall"))
        for feature in features :
            res[target][feature] = correlations[feature]
            
    return res

def plotKendallCoeffs(coeffs,saveAs=False) :
    
    # converting coefficients into a dataframe to then use pandas.Dataframe.plot.bar    
    targets = list(coeffs.keys())
    features = list(coeffs[list(coeffs.keys())[0]].keys())
    
    data = np.zeros((len(targets),len(features)))
    for indTarget,target in enumerate(targets) :
        for indFeature,feature in enumerate(features) :
            data[indTarget,indFeature] = coeffs[target][feature]
    
    # Creating the dataframe
    df = pd.DataFrame(data, columns=features,index=targets)
    
    # Creating the plot
    plot = df.plot.bar(rot=0,ylim=(-1.3,1),figsize=(18,10))
    plot.axhline(-1, color='grey', linewidth=1)
    plot.axhline(0, color='grey', linestyle='dashed', linewidth=1)
    plot.legend(loc='lower center', frameon=False,title="Kendall Correlation Analysis",ncol=(len(features)+2)//3)

    fig = plot.get_figure()
    
    # Saving the plot if desired
    if saveAs :
        fig.savefig(saveAs)
        print("Plot saved as",saveAs)



if __name__ == "__main__" :

    df = pd.read_csv("dataReport.csv",sep=",")

    coeffs = getKendallCoeffs(df,features=['__activation', '__batch_size','__dataset','__flattener','__loss','__optimizer','__useBatchNorm'],targets=["plateauLabel"])
    print(coeffs)
    plotKendallCoeffs(coeffs,saveAs="kendall.png")

