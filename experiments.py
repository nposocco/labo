# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 14:29:37 2019

@author: nposocco
"""

import time
import pandas as pd
import os
import labo.iocsv
import h5py
import numpy as np

# used to check python version
import sys
if sys.version_info[:1] == (2,) :
    import imp
elif sys.version_info[:2] in [(3,3),(3,4)] :
    from importlib.machinery import SourceFileLoader
else : # 3.5+
    import importlib.util
    
import shutil

from keras.utils import plot_model

# used to build job parametrisations
from labo.parameters import buildAllCombinations

# used to perform analyses
from labo.analysis.pearson import getPearsonCoeffs,plotPearsonCoeffs
from labo.analysis.spearman import getSpearmanCoeffs,plotSpearmanCoeffs
from labo.analysis.kendall import getKendallCoeffs,plotKendallCoeffs

# used to prettify analyses results in txt files
import pprint

# used to save errors caught during execution of a protocol
import traceback

import warnings
warnings.filterwarnings("ignore")

# EXPERIMENT_FILE
# -> results
#    dataReport.csv
#    -> jobs
#       -> <i> job
# -> analyses
#    -> <date>_<analysis>
#       -> fichiers relatifs à l'analyse
# -> protocoles
#    -> <date>_protocol.py


# TO DO
# ajouter le labeliser
#/ intégrer l'analyse de Spearman
#/ intégrer l'analyse de Kendall
#/ ajouter une gestion d'erreurs lors de l'execution d'un job
#
# faire en sorte que les métriques soient sauvegardées dans le csv et pas en dehors
# ajouter l'analyse de Shapley estimée
# ajouter l'analyse de Shapley
# faire en sorte de fixer les seeds
# ajouter des logs

def merge(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z



def buildNextParameters(currentParameters,parameters,parametersIndex) :
    # Choisit les prochains paramètres d'expérience
    # Retourne False si tous les paramètres ont été testés

    # if it is the first job
    if currentParameters == {} :
        for key in parametersIndex :
            currentParameters[key] = parameters[key][0]
    else :
        for i,key in enumerate(parametersIndex) :
            current_parameter_index = parameters[key].index(currentParameters[key])
            if current_parameter_index < len(parameters[key])-1 :
                currentParameters[key] = parameters[key][current_parameter_index+1]
                break
            else :
                if i == len(parametersIndex)-1 :
                    return False
                currentParameters[key] = parameters[key][0]
    return currentParameters

class Experiment(object) :

    def __init__(self,
                 parameters,
                 name,
                 directory="experiments/",
                 ) :
        
        # preventing a problem if the experiment already exists
        # (allows multiple LabTechnicians to work on the same experiment without changing anything in the code)
        if os.path.exists(os.path.join(directory,name)):
            return
        
        self.name = name
        self.parameters = parameters
        
        # Creating folder for experiment
        self.directory = directory
        if not os.path.exists(os.path.join(self.directory,name)):
            os.makedirs(os.path.join(self.directory,name))

        # Creating folder for results
        if not os.path.exists(os.path.join(self.directory,name,"results")):
            os.makedirs(os.path.join(self.directory,name,"results"))
            
        # Creating folder for jobs
        if not os.path.exists(os.path.join(self.directory,name,"results","jobs")):
            os.makedirs(os.path.join(self.directory,name,"results","jobs"))

        # Creating folder for analysis
        if not os.path.exists(os.path.join(self.directory,name,"analyses")):
            os.makedirs(os.path.join(self.directory,name,"analyses"))

        # Creating folder for protocols
        if not os.path.exists(os.path.join(self.directory,name,"protocols")):
            os.makedirs(os.path.join(self.directory,name,"protocols"))
            
        # Copying protocol file
        shutil.copy2("protocol.py",os.path.join(self.directory,self.name,"protocols","protocol_"+time.strftime("%Y%m%d-%Hh%Mm%Ss")+".py"))
        
        # initializing the csv report
        df0 = pd.DataFrame([{"__jobID":i} for i in range(len(parameters))])
        df1 = pd.DataFrame([{"__status":"waiting"} for i in range(len(parameters))])
        df2 = pd.DataFrame([{"__protocol_version":"not done"} for i in range(len(parameters))])
        df3 = pd.DataFrame([{"__execution_time":"not done"} for i in range(len(parameters))])
        rows = []
        for row in parameters :
            newRow = {}
            for param in row :
                newRow["__"+str(param)] = row[param]
            rows.append(newRow)
        df4 = pd.DataFrame(rows)
        pd.concat([df0,df1,df2,df3,df4], axis = 1).to_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), index=False)
        
    def getLabTechnician(self) :
        return LabTechnician(self.name,directory=self.directory)


    
    
    
class LabTechnician(object) :
    
    def __init__(self,
                 experimentName,
                 directory="experiments/") :
        self.name = experimentName
        self.directory = directory
        self.currentParameters = {}
        self.current_job_id = -1
        
        
        
        self.parameters = []
        experimentData = pd.read_csv(os.path.join(self.directory,experimentName,"results","dataReport.csv"), sep=",")
        for column in experimentData.columns :
            if column[:2] == "__" and column not in ("__jobID","__status","__protocol_version","__execution_time") :
                self.parameters.append(column)
        
    def work(self,rebootErrors=False) :
        
        if rebootErrors :
            print("\nRebooting failed jobs...")
            experiment_data = pd.read_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), sep=",")
            experiment_data.loc[experiment_data['__status'] == "failed", '__status'] = "waiting"
            experiment_data.to_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), index=False)
            print("Done.")
            
        print("\nStarting Working")
        while self.determineNextParameters() != False :
            
            print("\nCurrent job  :{:>10}".format(self.current_job_id))
            print("Parameters :",self.currentParameters)
            
            print("Creating job directory")
            if not os.path.exists(os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id))):
                os.makedirs(os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id)))
                os.makedirs(os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id),"error_logs"))

            print("Loading latest protocol")
            try :
                latest = os.path.splitext(sorted(os.listdir(os.path.join(self.directory,self.name,"protocols")))[-1])[0]
                print(latest)
                if sys.version_info[:1] == (2,) :
                    file_, pathname, description = imp.find_module(latest, [os.path.join(self.directory,self.name,"protocols")])
                    protocolModule = imp.load_module('protocol', file_, pathname, description)
                    # OLD protocolModule = imp.load_source('protocol', os.path.join(self.directory,self.name,"protocols",latest))
                elif sys.version_info[:2] in [(3,3),(3,4)] :            
                    protocolModule = SourceFileLoader('protocol', os.path.join(self.directory,self.name,"protocols",latest)).load_module()
                else : # 3.5+
                    spec = importlib.util.spec_from_file_location('protocol', os.path.join(self.directory,self.name,"protocols",latest+".py"))
                    protocolModule = importlib.util.module_from_spec(spec)
                    spec.loader.exec_module(protocolModule)
            except Exception as e:
                print("Protocol LOADING has failed !\nDetails can be found in job directory.")
                #print("Protocol loading has failed !\nDetails can be found in job directory.")
                #template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                #message = template.format(type(e).__name__, e.args)
                
                errorTime = str(time.asctime( time.localtime(time.time()) )).replace(":","_")
                os.makedirs(os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id),"error_logs",errorTime))
                
                message = traceback.format_exc()
                with open(os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id),"error_logs",errorTime,"loading_traceback.txt"),"w") as errorFile :
                    errorFile.write(message)
                
                experiment_data = pd.read_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), sep=",")
                experiment_data.set_value(self.current_job_id, "__status", "failed")
                experiment_data.to_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), index=False)
                continue
                
            print("Starting job with parameters : ")
            print(self.currentParameters)
            
            
            try :
                startTime = time.time()
                protocolModule.protocol(experiment=self,**self.currentParameters)
                endingTime = time.time()
            except Exception as e :
                print("Protocol EXECUTION has failed !\nDetails can be found in job directory.")
                message = traceback.format_exc()
                
                errorTime = str(time.asctime( time.localtime(startTime) )).replace(":","_")
                os.makedirs(os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id),"error_logs",errorTime))
                
                with open(os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id),"error_logs",errorTime,"execution_traceback.txt"),"w") as errorFile :
                    errorFile.write(message)
                experiment_data = pd.read_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), sep=",")
                experiment_data.set_value(self.current_job_id, "__status", "failed")
                
                # sauvegarde de la version du protocole, puis sauvegarde de la ligne du csv du job dans le dossier d'erreur
                experiment_data.set_value(self.current_job_id, "__protocol_version", latest.split("_")[1])
                experiment_data[experiment_data["__jobID"] == self.current_job_id].sample(1).to_csv(os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id),"error_logs",errorTime,"csv_state.csv"), index=False)
                
                # clearing job line, to prevent conflicts with a future job attempt
                for column in list(experiment_data) :
                    if column[:2] != "__" :
                        experiment_data.set_value(self.current_job_id, column, None)
                experiment_data.set_value(self.current_job_id, "__protocol_version", "not done")
                # la clear sauf au niveau de ce qui commence par __ et du status
                experiment_data.to_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), index=False)
                continue
            
            print("End of Job at time : ",time.asctime( time.localtime(endingTime) ))
            print("Job has taken",endingTime-startTime,"seconds")
            experiment_data = pd.read_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), sep=",")
            experiment_data.set_value(self.current_job_id, "__status", "done")
            experiment_data.set_value(self.current_job_id, "__protocol_version", latest.split("_")[1])
            experiment_data.set_value(self.current_job_id, "__execution_time", endingTime-startTime)
            experiment_data.to_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), index=False)
            
    def saveValue(self,column,value) :
        """
        Writes in a cell of the experiment report data file
        """
        if column[:2] == "__" :
            raise Exception("Experiment parameters may not be overwritten by experiments.")
        experiment_data = pd.read_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), sep=",")
        if column not in list(experiment_data) :
            experiment_data[column] = None
        experiment_data.set_value(self.current_job_id, column, value)
        experiment_data.to_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), index=False)
        
    def determineNextParameters(self) :
        experiment_data = pd.read_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), sep=",")
        if len(experiment_data[experiment_data["__status"] == "waiting"]) == 0 :
            return False
        todo = experiment_data[experiment_data["__status"] == "waiting"].sample(1).iloc[0]["__jobID"]
        experiment_data.set_value(todo, "__status", "working")
        experiment_data.to_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), index=False)
        self.current_job_id = todo
        params = {}
        for param in self.parameters :
            params[param[2:]] = experiment_data[experiment_data["__jobID"] == self.current_job_id].iloc[0][param]
        self.currentParameters = params
            
    def getCurrentJobDirectory(self) :
        return os.path.join(self.directory,self.name,"results","jobs",str(self.current_job_id))
                    
    def saveModel(self,model,name="model") :
        """
        Saves a png and a json representation of the given model in the current job directory
        """
        model_json = model.to_json()
        plot_model(model, show_shapes = True, to_file=self.getCurrentJobDirectory()+"/"+'model.png')
        with open(self.getCurrentJobDirectory()+"/"+name+".json", "w") as json_file:
            json_file.write(model_json)
        
    def loadDataIndex(self,path,setFunction=None) :
        """
        Loads a list of example indexes and copies index file into current job directory
        """
        dataIndex = labo.iocsv.readListCsv(path)
        if setFunction != None :
            labo.iocsv.saveListCsv(dataIndex, os.path.join(self.getCurrentJobDirectory(),setFunction+'.csv'))
        return dataIndex
    
    def createSetFromIndex(self,dataIndex,shape,all_set,labels,normalization) :
        nbr_examples = len(dataIndex)
        X = np.zeros([nbr_examples] + shape, dtype=np.float32)
        Y = np.zeros([nbr_examples, 1], dtype=np.int)     
        for i, filename in enumerate(dataIndex):              
            #load data
            print('file {}/{} : {}'.format(i+1, len(dataIndex), filename))
            data = all_set[int(filename)]       
            #normalize
            if normalization == 'minMax':
                minData = np.amin(data)
                maxData = np.amax(data)
                data = (data - minData)*1. / (maxData - minData) 
            #store in X and Y
            X[i] = np.array(data) 
            Y[i] = labels[int(filename)]
           
        print("normalize...")
    
        if normalization == 'percentile':
            minData = np.percentile(X,1)
            maxData = np.percentile(X,99)
            X = (X - minData)*1. / (maxData - minData)
            
        elif normalization == 'meanstd':
            std_X = np.std(X)
            X = (X - np.mean(X))*1. / std_X
        
        
        X = np.expand_dims(X,axis=-1)
           
        return (X, Y)
    
    def createSetFromIndexes(self,dataPath,indexes,Xlabels,Ylabels,shape,normalization) :
        
        # loading data file and keeping columns of interest for features and targets
        f = h5py.File(dataPath,'r')
        Xcolumns,Ycolumns = f[Xlabels],f[Ylabels]
        
        # creating sets from indexes
        res = []
        for index in indexes :
            X_train, Y_train = self.createSetFromIndex(index,shape,Xcolumns,Ycolumns,normalization)
            res.append((X_train, Y_train))
        f.close()
        return res
    
    def execute(self,fct) :
        experimentData = pd.read_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), sep=",")
        jobIDs = experimentData["__jobID"].tolist()
        print("\nExecuting a function on"+str(len(jobIDs))+"jobs...")
        for ID in jobIDs :
            print("\nApplying function on job #"+str(ID),"...")
            self.current_job_id = ID
            params = {}
            for param in self.parameters :
                params[param[2:]] = experimentData[experimentData["__jobID"] == self.current_job_id].iloc[0][param]
            self.currentParameters = params
            res = fct(self)
            print("Done. Saving results...")
            for key in res :
                self.saveValue(key,res[key])
            print("Done.")
        print("The function has been applied on all jobs...")
        
    def initializeExperimentAnalysis(self,name,startTime) :
        print("\nCreating folder for analysis",name)
        if not os.path.exists(os.path.join(self.directory,self.name,"analyses",startTime+"_"+name)):
            os.makedirs(os.path.join(self.directory,self.name,"analyses",startTime+"_"+name))
        shutil.copy2(os.path.join(self.directory,self.name,"results","dataReport.csv"),os.path.join(self.directory,self.name,"analyses",startTime+"_"+name,"dataReport.csv"))
        
    def performPearsonAnalysisOn(self,column) :

        startTime = time.strftime("%Y%m%d-%Hh%Mm%Ss")
        
        # Creating analysis directory and copying data
        analysisName = "Pearson_on_"+column
        self.initializeExperimentAnalysis(analysisName,startTime)
        
        # Loading experiment data
        print("Loading experimental data")
        experimentData = pd.read_csv(os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"dataReport.csv"), sep=",")

        # Calculating Pearson correlation coefficients
        print("Performing analysis")
        coeffs = getPearsonCoeffs(experimentData,features=self.parameters,targets=[column])
        
        # Saving results
        print("Saving results in directory",os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName))
        with open(os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"global_interpretation.txt"),"w") as textFile :
            pprint.pprint(coeffs, textFile)
        plotPearsonCoeffs(coeffs,saveAs=os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"global_interpretation.png"))

    def performSpearmanAnalysisOn(self,column) :

        startTime = time.strftime("%Y%m%d-%Hh%Mm%Ss")
        
        # Creating analysis directory and copying data
        analysisName = "Spearman_on_"+column
        self.initializeExperimentAnalysis(analysisName,startTime)
        
        # Loading experiment data
        print("Loading experimental data")
        experimentData = pd.read_csv(os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"dataReport.csv"), sep=",")

        # Calculating Spearman rank correlation coefficients
        print("Performing analysis")
        coeffs = getSpearmanCoeffs(experimentData,features=self.parameters,targets=[column])
        
        # Saving results
        print("Saving results in directory",os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName))
        with open(os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"global_interpretation.txt"),"w") as textFile :
            pprint.pprint(coeffs, textFile)
        plotSpearmanCoeffs(coeffs,saveAs=os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"global_interpretation.png"))

    def performKendallAnalysisOn(self,column) :

        startTime = time.strftime("%Y%m%d-%Hh%Mm%Ss")
        
        # Creating analysis directory and copying data
        analysisName = "Kendall_on_"+column
        self.initializeExperimentAnalysis(analysisName,startTime)
        
        # Loading experiment data
        print("Loading experimental data")
        experimentData = pd.read_csv(os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"dataReport.csv"), sep=",")

        # Calculating Kendall correlation coefficients
        print("Performing analysis")
        coeffs = getKendallCoeffs(experimentData,features=self.parameters,targets=[column])
        
        # Saving results
        print("Saving results in directory",os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName))
        with open(os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"global_interpretation.txt"),"w") as textFile :
            pprint.pprint(coeffs, textFile)
        plotKendallCoeffs(coeffs,saveAs=os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"global_interpretation.png"))
        
        
        
    
    def performShapleyAnalysisEstimationOn(self,column) :

        startTime = time.strftime("%Y%m%d-%Hh%Mm%Ss")
        
        # Creating analysis directory and copying data
        analysisName = "ShapleyEstimation_on_"+column
        self.initializeExperimentAnalysis(analysisName,startTime)
        
        # Determining all distinct rows
        experimentData = pd.read_csv(os.path.join(self.directory,self.name,"analyses",startTime+"_"+analysisName,"dataReport.csv"), sep=",")
        dataDistinct = experimentData[self.parameters].drop_duplicates()
        
        # Adding to this new dataframe the concensus target value
        
        # Calculating Shapley values
        
        # Saving global interpretation plots
        
        pass
    
    def updateProtocol(self) :
        print("Updating experiment protocol")
        shutil.copy2("protocol.py",os.path.join(self.directory,self.name,"protocols","protocol_"+time.strftime("%Y%m%d-%Hh%Mm%Ss")+".py"))
        print("Done.")

    def addAllExperimentCombinationsWith(self,experimentParams,amount) :

        # Normalizing experimentParams
        for key in experimentParams :
            if type(experimentParams[key]) is not list :
                experimentParams[key] = [experimentParams[key]]
    
        # Getting all possible values for the parameters
        experimentData = pd.read_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), sep=",")
        parameters = {}
        for parameter in self.parameters :
            parameters[parameter] = experimentData[parameter].unique().tolist()
            
        # Creating new job parameters instructions
        print("\nCreating new jobs with new given parameters")
        for parameter in experimentParams :
            parameters["__"+parameter] = experimentParams[parameter]
        linesToAdd = buildAllCombinations(parameters,amount=amount)
        
        # Getting the job with highest ID
        maxID = experimentData["__jobID"].max()
        
        # Adding them to the dataReport
        print("Adding them to the dataReport")
        df0 = pd.DataFrame([{"__jobID":maxID+1+i} for i in range(len(linesToAdd))])
        df1 = pd.DataFrame([{"__status":"waiting"} for i in range(len(linesToAdd))])
        df2 = pd.DataFrame([{"__protocol_version":"not done"} for i in range(len(linesToAdd))])
        df3 = pd.DataFrame([{"__execution_time":"not done"} for i in range(len(linesToAdd))])
        rows = []
        for row in linesToAdd :
            newRow = {}
            for param in row :
                newRow[str(param)] = row[param]
            rows.append(newRow)
        df4 = pd.DataFrame(rows)

        df = pd.concat([df0,df1,df2,df3,df4], axis=1)
        df = experimentData.append(df, ignore_index=True)
        df.to_csv(os.path.join(self.directory,self.name,"results","dataReport.csv"), index=False)
        print("Done.")
    
    # Aliases
    def getCJD(self) :
        return self.getCurrentJobDirectory()





        
        
