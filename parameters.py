# -*- coding: utf-8 -*-
"""
Created on Wed May  8 15:39:38 2019

@author: nicop
"""
import copy

# TO DO
# ajouter les rules


def buildAllCombinations(paramsDict,amount=1) :
    
    def buildNextParameters(currentParameters) :
        # Choisit les prochains paramètres d'expérience
        # Retourne False si tous les paramètres ont été testés
    
        # if it is the first job
        if currentParameters == {} :
            for key in parametersIndex :
                currentParameters[key] = paramsDict[key][0]
        else :
            for i,key in enumerate(parametersIndex) :
                current_parameter_index = paramsDict[key].index(currentParameters[key])
                if current_parameter_index < len(paramsDict[key])-1 :
                    currentParameters[key] = paramsDict[key][current_parameter_index+1]
                    break
                else :
                    if i == len(parametersIndex)-1 :
                        return False
                    currentParameters[key] = paramsDict[key][0]
        return currentParameters

    # Normalizing (if a parameter mau have only one value, convert it to list with one element)
    for key in paramsDict :
        if type(paramsDict[key]) is not list :
            paramsDict[key] = [paramsDict[key]]
    
    rows_list = []
    parametersIndex = [key for key in paramsDict]
    
    currentParameters = buildNextParameters({})
    while currentParameters != False :
        rows_list.append(currentParameters.copy())
        currentParameters = buildNextParameters(currentParameters)
        
    result = []
    for i in range(amount) :
        result.extend(copy.deepcopy(rows_list))
        
    return result
